program hello_world
  !$ use omp_lib 
  use mesh_class
  use data_desc_class
#ifdef MPI_MOD
  use mpi
#endif
  implicit none
#ifdef MPI_H
  include 'mpif.h'
#endif

  ! Locals
  integer :: info, me, np, tid

  call mpi_init(info) 
  if (info /= mpi_success) then
     write(0,*) 'Error in initalizing MPI, bailing out',info 
     call mpi_abort(mpi_comm_world, 1, info) 
  end if

  call mpi_comm_size(mpi_comm_world,np,info)
  call mpi_comm_rank(mpi_comm_world,me,info)
  
  !$OMP PARALLEL PRIVATE(tid) SHARED (me,np) 
  tid = 0
  !$ tid = omp_get_thread_num()
  write(*,'(a,i2,a,i2,a,i2,a)') 'Hello world from thread', tid, ' process id', me, ' of', np, ' processors'
  !$OMP END PARALLEL

  call mpi_finalize(info)
end program hello_world
