module matrix_vector
  use matrix_class
  use vector_class
  implicit none
contains
  subroutine matvec ( A, x, y )
    implicit none
    ! Parameters 
    type(matrix), intent(in)    :: A 
    type(vector), intent(in)    :: x
    type(vector), intent(inout) :: y
    
    y%data = 0.0d
    
    ! Locals
    integer              :: ierr, j

    ! *** Master on Numerical Methods in Engineering (MNME) ***
    ! *** Module Name: Domain Decomposition and Large Scale Scientific Computing (DDLSSC) ***
    ! *** TASK #5
    ! *** CODE DEVELOPMENT REQUIRED ***
    !     THE BODY OF THE FOLLOWING 
    !     SUBROUTINE MUST BE DEVELOPED
    !     FOR THE FINAL ASSIGNMENT
    
    #ifdef ENABLE_OPENMP
        
        ! Parallel execution , splitting do-iterations into blocks and then mapping these 
        !blocks to threads using a STATIC schedule approach
        !$OMP PARALLEL DEFAULT(NONE) SHARED (A,x,y) PRIVATE(j,yi)
         
        ! Define length sum vector and null-value of its starting components
        call vector_create(A%desc,yi)
        yi = 0.d0
        
        !$OMP DO SCHEDULE(STATIC)
        do j=1,A%m
            yi%data(1:A%n)=yi%data(1:A%n)+A%data(1:A%n,j)*x%data(j)
        end do
        !$OMP END DO
        
        ! Synchronizes the threads
        !$OMP CRITICAL Task5
        y%data(1:A%n) =  y%data(1:A%n) + yi%data(1:A%n)
        !$OMP END CRITICAL Task5
        
        ! Destroys yi
        call vector_destroy(yi)
        
        !$OMP END PARALLEL
    
    #else
        do j=1,A%m
            y%data(1:A%n)=A%data(1:A%n,j)*x%data(j)
        end do
    
    #endif
        
  end subroutine matvec


end module matrix_vector
